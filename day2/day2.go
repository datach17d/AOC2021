package day2

import (
	"gitlab.com/datach17d/AOC2021/utils"
)

type AOCDay2 struct {
	utils.AOCDay
	Params struct {
		Aim int
	}
}

func (day *AOCDay2) Do() int {

	curPos := [2]int{0, 0}
	aim := 0

	for directionVectors := range utils.ParseDirections(day.AOCDay.RawData) {
		curPos[0] += directionVectors[0]
		if day.Params.Aim == 0 {
			curPos[1] += directionVectors[1]
		} else {
			aim += directionVectors[1]
			curPos[1] += directionVectors[0] * aim
		}

	}
	return curPos[0] * curPos[1]
}
