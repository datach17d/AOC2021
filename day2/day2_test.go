package day2

import (
	"testing"

	"gitlab.com/datach17d/AOC2021/utils"
)

type day2Result struct {
	input string
	aim   int
}

var test_cases_1st_part = map[int]day2Result{
	150: {
		`forward 5
down 5
forward 8
up 3
down 8
forward 2`, 0},
	900: {`forward 5
down 5
forward 8
up 3
down 8
forward 2`, 1},
}

func TestExampleInput(t *testing.T) {
	for k, v := range test_cases_1st_part {
		day := AOCDay2{utils.AOCDay{RawData: v.input}, struct{ Aim int }{v.aim}}
		result := day.Do()
		if result != k {
			t.Fatalf("Expecting %d got %d", k, result)
		}
	}
}
