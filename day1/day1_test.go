package day1

import (
	"testing"

	"gitlab.com/datach17d/AOC2021/utils"
)

type day1Result struct {
	input  string
	window int
}

var test_cases_1st_part = map[int]day1Result{
	7: day1Result{`199
200
208
210
200
207
240
269
260
263`, 1},
	4: day1Result{`1
1
2
2
3
3
4
4
5
5`, 1},
	5: day1Result{`199
200
208
210
200
207
240
269
260
263`, 3},
}

func TestExampleInput(t *testing.T) {
	for k, v := range test_cases_1st_part {
		day := AOCDay1{utils.AOCDay{RawData: v.input}, struct{ Window int }{v.window}}
		result := day.Do()
		if result != k {
			t.Fatalf("Expecting %d got %d", k, result)
		}
	}
}
