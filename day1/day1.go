package day1

import (
	"gitlab.com/datach17d/AOC2021/utils"
)

type AOCDay1 struct {
	utils.AOCDay
	Params struct {
		Window int
	}
}

func (day1 *AOCDay1) Do() int {

	prev_depth_sum := -1
	depth_sum := -1
	depth_increase := 0
	depth := make([]int, day1.Params.Window)
	depth_count := 0

	for depth_value := range utils.ParseIntArray(day1.AOCDay.RawData) {

		depth[depth_count%day1.Params.Window] = depth_value
		depth_count++

		if depth_count >= day1.Params.Window {
			if depth_sum >= 0 {
				prev_depth_sum = depth_sum
			}
			depth_sum = 0
			for _, v := range depth {
				depth_sum += v
			}
		}

		if prev_depth_sum >= 0 {
			if depth_sum > prev_depth_sum {
				depth_increase++
			}
		}
	}

	return depth_increase
}
