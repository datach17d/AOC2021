package main

import (
	"os"
	"reflect"

	"gitlab.com/datach17d/AOC2021/day1"
	"gitlab.com/datach17d/AOC2021/day2"
	"gitlab.com/datach17d/AOC2021/utils"
)

func main() {
	utils.RunDay(os.Args[0],
		os.Args[1:],
		map[string]reflect.Type{
			"day1": reflect.TypeOf(day1.AOCDay1{}),
			"day2": reflect.TypeOf(day2.AOCDay2{}),
		},
		map[string]interface{}{})
}
