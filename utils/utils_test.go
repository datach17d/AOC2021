package utils

import (
	"errors"
	"reflect"
	"testing"
)

var int_data string = `199
200
208
210
200
207
240
269
260
263`

func TestParseIntArray(t *testing.T) {
	output := ChanToSlice(ParseIntArray(int_data))
	if !reflect.DeepEqual(output, []int{199, 200, 208, 210, 200, 207, 240, 269, 260, 263}) {
		t.Fatalf("Failed to parse array %#d", output)
	}
}

var directon_data = `forward 5
down 5
forward 8
up 3
down 8
forward 2`

func TestParseDirections(t *testing.T) {
	output := ChanToSlice(ParseDirections(directon_data))
	if !reflect.DeepEqual(output, [][2]int{{5, 0}, {0, 5}, {8, 0}, {0, -3}, {0, 8}, {2, 0}}) {
		t.Fatalf("Failed to parse array %#d", output)
	}
}

func TestFileLoad(t *testing.T) {
	output := AOCDay{}
	err := output.LoadData("int_data.txt")

	if err != nil {
		t.Fatalf("Failed to load file %#v", err)
	}

	if output.RawData != int_data {
		t.Fatalf("Failed to load file conents: %#v", output.RawData)
	}
}

type AOCDayTest struct {
	AOCDay
	Params struct {
		T            *testing.T
		TestString   string
		TestInt      int
		TestRawInput string
	}
}

func (dayTest *AOCDayTest) Do() error {
	err := errors.New("Fatal")
	if dayTest.Params.T != nil {
		err = nil

		if dayTest.Params.TestRawInput != dayTest.RawData {
			dayTest.Params.T.Fatalf("Input param TestRawInput wrong: %#v", dayTest.Params.TestRawInput)
		}
		if dayTest.Params.TestString != "test" {
			dayTest.Params.T.Fatalf("Input param TestString wrong: %#v", dayTest.Params.TestString)
		}
		if dayTest.Params.TestInt != 5 {
			dayTest.Params.T.Fatalf("Input param TestInt wrong: %#v", dayTest.Params.TestInt)
		}
	}
	return err
}

func TestDoDayParams(t *testing.T) {
	day := AOCDayTest{AOCDay{RawData: "data123"}, struct {
		T            *testing.T
		TestString   string
		TestInt      int
		TestRawInput string
	}{t, "test", 5, "data123"}}
	result := day.Do()
	if result != nil {
		t.Fatalf("Got error: %#v", result)
	}
}

func TestRunDayFlags(t *testing.T) {
	RunDay("test",
		[]string{"-day", "dayTest", "-file", "test_day.txt", "-TestString", "test", "-TestRawInput", "data123", "-TestInt", "5"},
		map[string]reflect.Type{
			"dayTest": reflect.TypeOf(AOCDayTest{}),
		},
		map[string]interface{}{
			"T": t,
		})
}
