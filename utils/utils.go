package utils

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"
	"unsafe"
)

func ChanToSlice(ch interface{}) interface{} {
	chv := reflect.ValueOf(ch)
	slv := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(ch).Elem()), 0, 0)
	for {
		v, ok := chv.Recv()
		if !ok {
			return slv.Interface()
		}
		slv = reflect.Append(slv, v)
	}
}

func ParseIntArray(data string) <-chan int {
	out := make(chan int)
	go func() {
		defer close(out)
		for _, s := range strings.Split(strings.Replace(data, "\r\n", "\n", -1), "\n") {
			n, err := strconv.Atoi(s)
			if err == nil {
				out <- n
			}
		}
	}()
	return out
}

func ParseDirections(data string) <-chan [2]int {
	out := make(chan [2]int)
	go func() {
		defer close(out)
		for _, s := range strings.Split(strings.Replace(data, "\r\n", "\n", -1), "\n") {
			kv := strings.Split(s, " ")
			n, err := strconv.Atoi(kv[1])
			if err == nil {
				if kv[0] == "forward" {
					out <- [2]int{n, 0}
				} else if kv[0] == "up" {
					out <- [2]int{0, -n}
				} else if kv[0] == "down" {
					out <- [2]int{0, n}
				}
			}
		}
	}()
	return out
}

type AOCDay struct {
	RawData string
}

func (day *AOCDay) LoadData(file_name string) error {
	content, err := ioutil.ReadFile(file_name)

	if err == nil {
		day.RawData = string(content)
	}

	return err
}

func RunDay(progname string, args []string, dayMap map[string]reflect.Type, extraOpts map[string]interface{}) {
	flags := flag.NewFlagSet(progname, flag.ContinueOnError)
	var buf bytes.Buffer
	flags.SetOutput(&buf)

	dayStr := flags.String("day", "day1", "AOC day to run")
	fileName := flags.String("file", "data/day1.txt", "Input file to use")

	flags.Parse(args)

	if val, ok := dayMap[*dayStr]; ok {
		dayValue := reflect.New(val)
		loadMethod := dayValue.MethodByName("LoadData")
		loadMethod.Call([]reflect.Value{reflect.ValueOf(*fileName)})

		ParamsValue, _ := val.FieldByName("Params")
		for i := 0; i < ParamsValue.Type.NumField(); i++ {
			ParamField := ParamsValue.Type.Field(i)
			ParamName := ParamField.Name
			optField := reflect.Indirect(dayValue).FieldByName("Params").FieldByName(ParamName)

			if val, ok := extraOpts[ParamName]; ok {
				optField.Set(reflect.ValueOf(val))
			} else {
				ParamTypeName := ParamField.Type.Name()
				if ParamTypeName == "string" {
					flags.StringVar((*string)(unsafe.Pointer(optField.Addr().Pointer())), ParamName, "", "")
				} else if ParamTypeName == "int" {
					flags.IntVar((*int)(unsafe.Pointer(optField.Addr().Pointer())), ParamName, 0, "")
				}
			}
		}

		flags.Parse(args)

		doMethod := dayValue.MethodByName("Do")
		resultVals := doMethod.Call([]reflect.Value{})
		for _, resultVal := range resultVals {
			fmt.Printf("%#v\n", resultVal)
		}
	}
}
